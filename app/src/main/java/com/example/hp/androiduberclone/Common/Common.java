package com.example.hp.androiduberclone.Common;

import android.location.Location;

import com.example.hp.androiduberclone.Model.User;
import com.example.hp.androiduberclone.Remote.FCMClient;
import com.example.hp.androiduberclone.Remote.IFCMService;
import com.example.hp.androiduberclone.Remote.IGoogleAPI;
import com.example.hp.androiduberclone.Remote.RetrofitClient;

public class Common {
    public static final String driver_pos_tbl = "ConductorPosicion";
    public static final String user_driver_tbl = "Conductor";
    public static final String user_rider_tbl = "Pasajero";
    public static final String estudiante = "Estudiante";
    public static final String pickup_request_tbl = "Viajes";
    public static final String token_tbl = "Tokens";

    public static User currentUser;
    public static String current_driver = null;

    public static Location mLastLocation = null;

    public static final String baseURL = "https://maps.googleapis.com";
    public static final String fcmURL = "https://fcm.googleapis.com/";

    public static IGoogleAPI getGoogleAPI()
    {
        return RetrofitClient.getClient(baseURL).create(IGoogleAPI.class);
    }

    public static IFCMService getFCMService()
    {
        return FCMClient.getClient(fcmURL).create(IFCMService.class);
    }
}
