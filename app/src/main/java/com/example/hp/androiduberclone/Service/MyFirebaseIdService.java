package com.example.hp.androiduberclone.Service;

import com.example.hp.androiduberclone.Common.Common;
import com.example.hp.androiduberclone.Model.Token;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseIdService extends FirebaseInstanceIdService{
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        updateTokenToServer(refreshedToken);// cuando tenga un token de actualización, debemos actualizar a nuestra base de datos en tiempo real

    }

    private void updateTokenToServer(String refreshedToken) {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        DatabaseReference tokens = db.getReference(Common.token_tbl);

        Token token = new Token(refreshedToken);
        if(FirebaseAuth.getInstance().getCurrentUser() != null)// Si ya inició sesión, debe actualizar el Token
            tokens.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .setValue(token);
    }
}
