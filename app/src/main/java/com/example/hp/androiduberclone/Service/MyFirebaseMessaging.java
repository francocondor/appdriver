package com.example.hp.androiduberclone.Service;

import android.content.Intent;
import android.util.Log;

import com.example.hp.androiduberclone.CustomerCall;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

public class MyFirebaseMessaging extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("DriverApp",remoteMessage.getNotification().getBody());
        // Porque enviare el mensaje the Firebase message conteniendo latitud y longitud del pasajero
        // Asi que necesitaré convertir el mensaje a LatLng
        LatLng customer_location = new Gson().fromJson(remoteMessage.getNotification().getBody(), LatLng.class);

        Intent intent = new Intent(getBaseContext(), CustomerCall.class);
        intent.putExtra("lat",customer_location.latitude);
        intent.putExtra("lng",customer_location.longitude);
        intent.putExtra("customer",remoteMessage.getNotification().getTitle());
        startActivity(intent);
    }
}
