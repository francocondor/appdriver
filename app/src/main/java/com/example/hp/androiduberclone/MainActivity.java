package com.example.hp.androiduberclone;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.hp.androiduberclone.Common.Common;
import com.example.hp.androiduberclone.Model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

    Button btnSignIn;
    FirebaseDatabase db;
    DatabaseReference users;
    RelativeLayout rootLayout;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Arkhip_font.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());

        setContentView(R.layout.activity_main);

        // Inicializar firebase
        db = FirebaseDatabase.getInstance();
        users = db.getReference(Common.user_driver_tbl);

        // Inicializar vista
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);


        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoginDialog();
            }
        });


//        Query query = db.getReference("Estudiantes").orderByKey().equalTo("201321239");
//        query.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                System.out.println("dataSnapshot: " + dataSnapshot);
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//            }
//        });
    }

    private void showLoginDialog() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("INICIA SESIÓN ");
        dialog.setMessage("Por favor use su usuario para iniciar sesión");

        LayoutInflater inflater = LayoutInflater.from(this);
        View login_layout = inflater.inflate(R.layout.layout_login, null);

        final MaterialEditText edtUsuario = login_layout.findViewById(R.id.edtUsuario);
        final MaterialEditText edtPassword = login_layout.findViewById(R.id.edtPassword);

        dialog.setView(login_layout);

        // Configurar el botón
        dialog.setPositiveButton("INGRESAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Chequea validación
                if (TextUtils.isEmpty(edtUsuario.getText().toString())) {
                    Snackbar.make(rootLayout, "Ingresa tu Usuario", Snackbar.LENGTH_SHORT).show(); return;
                }
                if (TextUtils.isEmpty(edtPassword.getText().toString())) {
                    Snackbar.make(rootLayout, "Ingresa tu clave", Snackbar.LENGTH_SHORT).show(); return;
                }
                if (edtPassword.getText().toString().length() < 6) {
                    Snackbar.make(rootLayout, "La Clave es muy corta !!!", Snackbar.LENGTH_SHORT).show(); return;
                }

                final android.app.AlertDialog waitingDialog = new SpotsDialog(MainActivity.this);
                waitingDialog.show();
                //Login
                Query query = db.getReference("Estudiantes").orderByKey().equalTo(edtUsuario.getText().toString());
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            Map<String, Object> newPost = (Map<String, Object>) dataSnapshot.getValue();
                            Map<String, Object> newPostCode = (Map<String, Object>) newPost.get(edtUsuario.getText().toString());
                            if (newPostCode.get("password").equals(edtPassword.getText().toString())) {
                                // VERIFICAR SI EXISTE EN "CONDUCTORES", SI NO EXISTE LO REGISTRAMOS
                                FirebaseDatabase.getInstance().getReference(Common.user_driver_tbl)
                                        .child(edtUsuario.getText().toString())
                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
//                                                Common.currentUser = dataSnapshot.getValue(User.class);
                                                Common.current_driver = edtUsuario.getText().toString();
                                                startActivity(new Intent(MainActivity.this, Welcome.class));
                                                finish();
                                            }

                                            @Override
                                            public void onCancelled(DatabaseError databaseError) {

                                            }
                                        });
                            } else {
                                Snackbar.make(rootLayout, "Clave incorrecta", Snackbar.LENGTH_SHORT).show();
                            }
                        } else {
                            Snackbar.make(rootLayout, "Usuario incorrecto", Snackbar.LENGTH_SHORT).show();
                        }
                        waitingDialog.dismiss();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        waitingDialog.dismiss();
                    }
                });

            }
        });

        dialog.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
