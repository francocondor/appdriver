package com.example.hp.androiduberclone.Remote;

import com.example.hp.androiduberclone.Model.FCMResponse;
import com.example.hp.androiduberclone.Model.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface IFCMService {
    @Headers({
            "Content-Type:application/json",
            "Authorization:key=AAAA3kQ64Pc:APA91bHQQDTtaqEQFWhefuRLoSDSWk2m8MoxXffgdNbD_JmdZpQfQcOn_2qpuJy3bQtlHKHr_Ccp41yq0OeJItbunUxcEqi2E_87x7A4bDIMziyBHc3XoZ13n3MfhOXp6R0c5lyuMYpU"
    })
    @POST("fcm/send")
    Call<FCMResponse> sendMessage(@Body Sender body);
}
